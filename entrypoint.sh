#!/usr/bin/env bash

main() {
  set -ex

  exec "$@"
}

main "$@"
