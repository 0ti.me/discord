# Discord Docker container

Hate Discord's bad privacy history? Throw it in docker and it'll hopefully be able to extract slightly less information from you.

## Assumptions

* Your distro uses x11.
* Your distro uses pulseaudio.
* You have a discord.deb file in the root of this directory, a sibling to this README.

## Steps

    curl -o discord.deb https://discordapp.com/api/download?platform=linux&format=deb
    docker-compose build
    docker-compose up -d
