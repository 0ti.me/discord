FROM debian:latest

ENV USER_NAME=discord

ENV \
  DEBIAN_FRONTEND=noninteractive \
  GROUP_ID=1000 \
  GROUP_NAME="${USER_NAME}" \
  HOME_DIR="/home/${USER_NAME}" \
  USER_ID=1000

RUN apt update && \
  apt install -y \
    gconf-service \
    gconf2-common \
    libappindicator1 \
    libc++1 \
    libc++1-7 \
    libc++abi1-7 \
    libdbus-glib-1-2 \
    libdbusmenu-glib4 \
    libdbusmenu-gtk4 \
    libgail-common \
    libgail18 \
    libgconf-2-4 \
    libgtk2.0-0 \
    libgtk2.0-bin \
    libgtk2.0-common \
    libgtk-3-dev \
    libindicator7 \
    libnotify4 notification-daemon \
    libnss3 \
    libxss-dev \
    pavucontrol \
    pulseaudio \
    python3 \
    python3-pip \
    python3-setuptools \
    wget

RUN \
  mkdir -p "${HOME_DIR}" \
    && groupadd -g "${GROUP_ID}" "${GROUP_NAME}" \
    && useradd -u "${USER_ID}" -g "${GROUP_NAME}" -d "${HOME_DIR}" "${USER_NAME}" \
    && chown -R "${USER_NAME}:${GROUP_NAME}" "${HOME_DIR}" \
    && chmod -R u+rw "${HOME_DIR}" \
    && chmod -R g-rwx "${HOME_DIR}" \
    && chmod -R o-rwx "${HOME_DIR}"

COPY ./entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

COPY discord.deb /tmp/discord.deb

RUN dpkg -i /tmp/discord.deb \
  && rm /tmp/discord.deb

COPY pulse-client.conf /etc/pulse/client.conf

USER "${USER_NAME}"

ENTRYPOINT ["/entrypoint.sh"]
CMD ["discord"]
